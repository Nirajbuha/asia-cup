import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./Home";
import India from './India';
import Pak from './pak';
import Lay from './Lay';
import Bang from './bang';
import Sri from './sri';
import { Table } from 'antd';
import News from './news';
import ScoreCard from './scorecrad';
import Update from './update';
import PlayerInfo from './rohit';





export default function App() {
  return (
    <BrowserRouter>
      <Routes>
      <Route path="/" element={<Lay />}>
        <Route path="/" element={<Home />} />
          <Route path="India" element={<India />} />
          <Route path="pakistan" element={<Pak />} />
          <Route path="bangladesh" element={<Bang />}></Route>
          <Route path='srilanka' element={<Sri />}></Route>
          <Route path='rohit' element={<PlayerInfo />}></Route>
          <Route path='news' element={<News />}></Route>
          <Route path='scorebox' element={<ScoreCard />}></Route>
          <Route path='updates' element={<Update />}></Route>
          
        </Route>
      </Routes>
    </BrowserRouter>
  );
}



