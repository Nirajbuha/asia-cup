import React from "react";
import Card from './card';
import idata from './idata';
import Note from './note';
import Footer from "./footer";




const India = () =>
    <>
        <div className="cards-container">
        {idata.map((val) => {
            return (
                
            
                <Card
                    id={val.id}
                    imgsrc={val.imgscr}
                    title={val.title}
                    sname={val.sname}
                    link={val.link}
                   
                />
             
            )
        }
    )}
       </div>
                
     <Note />
        <Footer />

    </>

export default India;
