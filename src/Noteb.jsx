import React from "react"


const Noteb = () => {
    return(

<div className='box1'>
<p className='mes'><strong className='mes1'>MEDIA ADVISORY</strong></p>
<p className='mes'><strong className='mes1'>Daka, 12 August 2023</strong></p><br />
<p className='mes'><strong className='mes1'>Bangladesh&rsquo;s squad for Asia Cup 2023 announced</strong></p>
<p className='mes'>The All-India Senior Selection Committee on Monday announced the BANGLADESH squad for the upcoming Asia Cup 2023.</p>
<p className='mes'><strong className='mes1'>Squad:&nbsp;</strong>Shakib Al Hasan (captain), Litton Das, Tanzid Tamim, Najmul Hossain Shanto, Towhid Hridoy, Mushfiqur Rahim, Mehidy Hasan Miraz, Taskin Ahmed, Mustafizur Rahman, Hasan Mahmud, Sheikh Mahedi, Nasum Ahmed, Shamim Hossain, Afif Hossain, Shoriful Islam, Ebadot Hossain, Naim Sheikh.</p>
<p className='mes'><strong className='mes1'>Non-traveling reserves:&nbsp;</strong>Taijul Islam, Saif Hasan, Tanzim Hasan Sakib<strong className='mes1'><br /></strong></p><br />
<p className='mes'><strong className='mes1'>NAZMUL HASSAN, MP</strong><br /><strong className='mes1'>PRESIDENT</strong><br /><strong className='mes1'>BCB</strong></p>
</div> 
    );
}

export default Noteb;