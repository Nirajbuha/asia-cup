import React from "react"


const Notep = () => {
    return(

<div className='box1'>
<p className='mes'><strong className='mes1'>MEDIA ADVISORY</strong></p>
<p className='mes'><strong className='mes1'>Lahore, 24 August 2023</strong></p><br />
<p className='mes'><strong className='mes1'>Pakistan&rsquo;s squad for Asia Cup 2023 announced</strong></p>
<p className='mes'>The All-India Senior Selection Committee on Monday announced the Pakistan squad for the upcoming Asia Cup 2023.</p>
<p className='mes'><strong className='mes1'>Squad:&nbsp;</strong>Qasim Akram (captain), Omair Bin Yousuf (vice-captain), Aamir Jamal, Arafat Minhas, Arshad Iqbal, Asif Ali, Haider Ali, Khushdil Shah, Mirza Tahir Baig, Mohammad Hasnain, Muhammad Akhlaq (wk), Rohail Nazir, Shahnawaz Dahani, Sufiyan Muqeem and Usman Qadir</p>
<p className='mes'><strong className='mes1'>Non-traveling reserves:&nbsp;</strong> Abdul Wahid Bangalzai, Mehran Mumtaz<strong className='mes1'><br /></strong></p><br />
<p className='mes'><strong className='mes1'>HASSAN CHEEMA</strong><br /><strong className='mes1'>Honorary Secretary</strong><br /><strong className='mes1'>PCB</strong></p>
</div> 
    );
}

export default Notep;