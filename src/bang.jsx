import React from "react";
import Card from './card';
import bdata from './badata';
import Noteb from "./Notep";
import Footer from "./footer";



const Bang = () =>
    <>
        <div className="cards-container">
            {bdata.map((val) => {
                return (
                    <Card
                        key={val.id}
                        imgsrc={val.imgscr}
                        title={val.title}
                        sname={val.sname}
                        link={val.link}
                    />

                )
            }
            )}
           
        </div>
        <Noteb />
        <Footer />
    </>
export default Bang;