import React from "react";
import { Link } from "react-router-dom";


function Card(prop) {
    return (
        <>
                <div className="card">
                    <img src={prop.imgsrc}
                        alt="mypqic"
                        className="card_img" />
                    <div className="card__info">
                        <span className="card__category">{prop.title}</span>
                        <h3 className='card__title'>{prop.sname}</h3>
                        <Link to={prop.link} state={{ id: prop.id}}>
                            <button className="button">About Career</button>
                        </Link>
                    </div>
                </div>
            
        </>
     );
}

export default Card;
