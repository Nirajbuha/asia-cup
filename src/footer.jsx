import React from "react";


const Footer = () => {
    return (
        <div className="footer">
            <h2 className="company_name">
                Crickinfo
            </h2>
            <p>copyright &copy; 2023 Crickinfo</p>
            <div className="lfooter"> 
            <p style={{marginLeft:"10px"}}>About us</p>
            <p style={{marginLeft:"10px"}}>|</p>
            <p style={{marginLeft:"10px"}} >privacy policy</p>
            <p style={{marginLeft:"10px"}}>|</p>
            <p style={{marginLeft:"10px"}}>security</p>
            <p style={{marginLeft:"10px"}}>|</p>
            <p style={{marginLeft:"10px"}}>Services</p>
            </div>
           
            
        </div>
    )
}
export default Footer;