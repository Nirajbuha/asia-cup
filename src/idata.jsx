

const tdata = [
    {

        id: 1,
        imgscr: 'https://pbs.twimg.com/media/FxrolxPaAAArbwh.jpg',
            title: "Rohit shrma (C)",
            sname: "BATSMAN",
            link: "/rohit", 
    },
    {
        id: 2,
        imgscr: "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_800,q_50/lsci/db/PICTURES/CMS/364400/364495.jpg",
            title: "Virat kohli",
            sname: "BATSMAN",
            link: '/rohit',
            
    },
    {
        id: 3,
        imgscr: "https://pbs.twimg.com/profile_images/1582311159910215683/doSdYa8e_400x400.jpg",
            title: "Suryakumar yadav",
            sname: "BATSMAN",
            link: 'https://www.espncricinfo.com/cricketers/suryakumar-yadav-446507',
          
    },
    {
        id: 4,
        imgscr: "https://www.hindustantimes.com/static-content/1y/cricket-logos/players/shreyas-iyer.png",
            title: "Shreyas Iyer",
            sname: "BATSMAN",
            link: 'https://www.hindustantimes.com/cricket/players/shreyas-iyer-63961',
          
    },
    {
        id: 5,
        imgscr: "https://www.hindustantimes.com/static-content/1y/cricket-logos/players/shubman-gill.png",
            title: "Shubman Gill",
            sname: "BATSMAN",
            link: 'https://www.hindustantimes.com/cricket/players/shubman-gill-66818',
          
    },
    {
        id: 6,
        imgscr: "https://pbs.twimg.com/media/F43EU3NXYAAH4X3?format=jpg&name=large",
        title: "Tilak Varma",
            sname: "BATSMAN",
            link: 'https://www.hindustantimes.com/cricket/players/tilak-varma-70761',
          
    },
    {
        id: 7,
        imgscr: "https://www.hindustantimes.com/static-content/1y/cricket-logos/players/axar-patel.png",
            title: "Axar Patel",
            sname: "ALL ROUNDER",
            link: 'https://www.hindustantimes.com/cricket/players/axar-patel-62576',
          
    },
    {
        id: 8,
        imgscr: "https://www.hindustantimes.com/static-content/1y/cricket-logos/players/hardik-pandya.png",
            title: "Hardik Pandya",
            sname: "ALL ROUNDER",
            link: 'https://www.hindustantimes.com/cricket/players/hardik-pandya-63751',
          
    },
    {
        id: 9,
        imgscr: "https://www.hindustantimes.com/static-content/1y/cricket-logos/players/ravindra-jadeja.png",
            title: "Ravindra Jadeja",
            sname: "ALL ROUNDER",
            link: 'https://www.hindustantimes.com/cricket/players/ravindra-jadeja-3850',
          
    },
    {
        id: 10,
        imgscr: "https://pbs.twimg.com/media/Fx8DAUaaIAAXcpc?format=jpg&name=large",
            title: "Ishan Kishan",
            sname: "WICKET KEEPER ",
            link: 'https://www.hindustantimes.com/cricket/players/ishan-kishan-64712',

    },
    {
        id: 11,
        imgscr: "https://pbs.twimg.com/media/En4eR5pVkAA33Ck?format=jpg&name=large  ",
            title: "Sanju Samson",
            sname: "WICKET KEEPER ",
            link: 'https://www.hindustantimes.com/cricket/players/sanju-samson-61837',
          
    },
    {
        id: 12,
        imgscr: "https://www.hindustantimes.com/static-content/1y/cricket-logos/players/jasprit-bumrah.png",
            title: "Jasprit Bumrah",
            sname: "BOWLER",
            link: 'https://www.hindustantimes.com/cricket/players/jasprit-bumrah-63755',
          
    },
    {
        id: 13,
        imgscr: "https://pbs.twimg.com/media/ExllikAUYAE0Meg?format=jpg&name=large",
            title: "Kuldeep Yadav",
            sname: "BOWLER",
            link: 'https://www.hindustantimes.com/cricket/players/kuldeep-yadav-63187',
          
    },
    {
        id: 14,
        imgscr: "https://pbs.twimg.com/media/ExGCB6jVkAE74Ji?format=webp&name=small",
            title: "Prasidh Krishna",
            sname: "BOWLER",
            link: 'https://www.hindustantimes.com/cricket/players/prasidh-krishna-65702',
          
    },
    {
        id: 15,
        imgscr: "https://www.hindustantimes.com/static-content/1y/cricket-logos/players/mohammad-shami.png",
            title: "Mohammad Shami",
            sname: "BOWLER",
            link: 'https://www.hindustantimes.com/cricket/players/mohammad-shami-28994',
    }
    

]
export default tdata;