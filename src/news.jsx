const News = () =>{
    return(
        <div className="mainbox">
            <h3 style={{padding:"20px"}}>NEWS</h3><hr />
        <h1>KL Rahul: 'I know what to do mentally when thrown into the ring'</h1><br />
        <p>The India batter on coming back from injury "without any baggage", his surprise inclusion in the XI against Pakistan, and moreKL Rahul wasn't in India's first XI for their Asia Cup Super Four fixture against Pakistan. But just prior to the toss in Colombo, when he was asked by head coach Rahul Dravid to get ready, there was a slight problem. Knowing he was going to carry drinks, Rahul had left his batting gear and kit back at the hotel. It needed frantic running around from the team manager to get his kit across to the R Premadasa Stadium</p>
        <br />
        <p>"Five minutes before toss, Rahul bhai told me you might have to play because Shreyas [Iyer] has a back spasm," Rahul told official broadcaster Star Sports after he marked his comeback with his sixth ODI hundred in India's</p>
        <br />
        <p>"At the last minute, our manager had to run to the hotel to get my stuff. Strange things have happened in my career. This isn't the first time. It has happened earlier too. Mentally I guess I know what to do when I'm thrown into the ring, I give my best. I'm happy such performances happen [when he's been faced with such situations]. Maybe it's also a sign that I don't have to think too much. I can just go out there and enjoy my cricket. That's the learning for me."</p>
        <br />
        <p>Rahul was right when he said such things had happened earlier too. One such moment, in 2016, perhaps changed the course of his career. At an IPL game in Rajkot against Gujarat Lions, Rahul wasn't listed in the XI for Royal Challengers Bangalore at toss time. But ten minutes prior to the start, he was seen rushing off the field to get ready after Mandeep Singh tore the webbing on his left hand. Virat Kohli then asked Suresh Raina, the opposition captain, if they could make a late change.</p>
        <br />
        <img className="news_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_w_1280,q_70/lsci/db/PICTURES/CMS/367000/367058.4.jpg" alt="" />
        <br />
        <br />
        <p>Rahul responded with a gutsy half-century at No. 4. It was the start of a golden run for him that season. From being a non-starter in the XI, Rahul became a regular as RCB made an inspired run to the final. While Kohli made a chart-topping 973 runs, Rahul contributed heavily too: 397 runs in 12 innings at a strike rate of 146.49. That season dispelled notions of him being a one-format player. In fact, it was also during that season when Rahul made a mark as a wicketkeeper, a role he has grown in over time. He would make his ODI and T20I debut for India soon after the IPL.</p>
        <br />
        <p>This ability to bat in the middle and keep wickets makes him a crucial part of India's ODI World Cup jigsaw. This is why the team management waited on him to return to full fitness five months after he limped off with an injured hamstring. It has taken a lot of work behind the scenes for Rahul to fully get ready before he joined the team last week ahead of the Super Fours.</p>
        <br />
        <p>"I've had three [tests] done in the last ten days," Rahul said with a laugh. "Two yo-yo tests, two practice matches, this [batting in the middle in humid conditions] was worse than that [yo-yo tests]. Yeah, for four months I've had a lot of juice, and energy. Hopefully, I can carry this, try to recover fast and come back fresh [for the Sri Lanka game].</p>
        <br />
       </div>
    )
}
export default News;