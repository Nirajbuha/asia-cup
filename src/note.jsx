import React from "react"


const Note = () => {
    return(

<div className='box1'>
<p className='mes'><strong className='mes1'>MEDIA ADVISORY</strong></p>
<p className='mes'><strong className='mes1'>August 21, 2023</strong></p><br />
<p className='mes'><strong className='mes1'>India&rsquo;s squad for Asia Cup 2023 announced</strong></p>
<p className='mes'>The All-India Senior Selection Committee on Monday announced the Indian squad for the upcoming Asia Cup 2023.</p>
<p className='mes'><strong className='mes1'>Squad:&nbsp;</strong>Rohit Sharma (Captain), Shubman Gill, Virat Kohli, Shreyas Iyer, Suryakumar Yadav, Tilak Varma, KL Rahul, Ishan Kishan, Hardik Pandya (VC), Ravindra Jadeja, Shardul Thakur, Axar Patel, Kuldeep Yadav, Jasprit Bumrah, Mohd. Shami, Mohd. Siraj, Prasidh Krishna.</p>
<p className='mes'><strong className='mes1'>Traveling stand-by player:&nbsp;</strong>Sanju Samson<strong className='mes1'><br /></strong></p><br />
<p className='mes'><strong className='mes1'>JAY SHAH</strong><br /><strong className='mes1'>Honorary Secretary</strong><br /><strong className='mes1'>BCCI</strong></p>
</div> 
    );
}

export default Note;