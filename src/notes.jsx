import React from "react"


const Notes = () => {
    return(

<div className='box1'>
<p className='mes'><strong className='mes1'>MEDIA ADVISORY</strong></p>
<p className='mes'><strong className='mes1'> 19 August 2023</strong></p><br />
<p className='mes'><strong className='mes1'>Sri lanka&rsquo;s squad for Asia Cup 2023 announced</strong></p>
<p className='mes'>The All-India Senior Selection Committee on Monday announced the Sri lanka squad for the upcoming Asia Cup 2023.</p>
<p className='mes'><strong className='mes1'>Squad:&nbsp;</strong>Dasun Shanaka (c), Pathum Nissanaka, Dimuth Karunaratne, Kusal Janith Perera, Kusal Mendis (vc), Charith Asalanka, Dhananjaya de Silva, Sadeera Samarawickrama, Maheesh Theekshana, Dunith Wellalage, Matheesha Pathirana, Kasun Rajitha, Dushan Hemantha, Binura Fernando.</p>
<p className='mes'><strong className='mes1'>Non-traveling reserves:&nbsp;</strong>  Pramod Madushan<strong className='mes1'><br /></strong></p><br />
<p className='mes'><strong className='mes1'>SHAMMI SILVA</strong><br /><strong className='mes1'>President</strong><br /><strong className='mes1'>PCB</strong></p>
</div> 
    );
}

export default Notes;