import React from "react";
import Card from './card';
import pdata from './pdata';
import Notep from "./Notep";
import Footer from "./footer";





const Pak = () =>
<>
<div className="cards-container">
        {pdata.map((val) => {
            return (
                <Card
                    key={val.id}
                    imgsrc={val.imgscr}
                    title={val.title}
                    sname={val.sname}
                    link={val.link}
                />
            
            )
        }
    )}
</div>
    <Notep />
    <Footer />

    </> 
  

export default Pak;