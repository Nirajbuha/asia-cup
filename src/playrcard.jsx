import React from "react";

function Player(prop) {
    return (
        <>
            <div className="fullbody">
                <div className="rohit_container">
                    <div className="name1">
                        <h1>{prop.name}</h1>
                        <h3>cricketer</h3>
                    </div>
                    <hr />

                    <div className="rohitdata_container">
                        <div className="rohit_photo">
                            <img src={prop.imgsrc}
                                alt="player img"
                                className="rohit_img" />
                        
                    </div>
                    <div className="rohit_data row">
                        <div className="player_details col">
                            <div className="player_colm col">
                                <h2>Name</h2>
                                <p className="details1">{prop.name}</p>
                            </div>
                            <div className="player_colm col">
                                <h2>Born</h2>
                                <p className="details1">{prop.born}</p>
                            </div>
                        </div>
                    </div>
                    <div className="rohit_data2">
                        <div className="player_details2">
                            <div className="player_colm2 ">
                                <h2>Born Place</h2>
                                <p className="details1">{prop.bornplace}</p>
                            </div>
                            <div className="player_colm2 ">
                                <h2>Batting style</h2>
                                <p className="details1">{prop.battingstyle}</p>
                            </div>
                        </div>

                    </div>
                    <div className="rohit_data3">
                        <div className="player_details3">
                            <div className="player_colm3 ">
                                <h2>Bowling Style</h2>
                                <p className="details1">{prop.bowlingstyle}</p>
                            </div>
                            <div className="player_colm3 ">
                                <h2>Nationality</h2>
                                <p className="details1">{prop.nationality}</p>
                            </div>
                        </div>
                    </div>
                    <div className="rohit_data3">
                        <div className="player_details3">
                            <div className="player_colm3 ">
                                <h2>Age</h2>
                                <p className="details1">{prop.age}</p>
                            </div>
                            <div className="player_colm3 ">
                                <h2 style={{paddingBottom:"7px"}}>Playing Role</h2>
                                <p className="details1">{prop.role}</p>
                            </div>
                        </div>
                    </div>
                </div>
<br />
                <hr />
                <div className="all_data">
                    <div className="player_data1">
                        <div className="player_team">
                            <div className="team_name">
                                <h2>Team</h2>
                                <p className="details1">{prop.team}</p>
                            </div>
                        </div>
                    </div>
                    <div className="player_data2">
                        <div className="player_role">
                            <div className="role_name">
                                <h2>Role</h2>
                                <p className="details1">{prop.role2}</p>
                            </div>
                        </div>
                    </div>
                    <div className="player_data3">
                        <div className="player_matches">
                            <div className="matches_name">
                                <h2>matches</h2>
                                <p className="details1">{prop.matches}</p>
                            </div>
                        </div>
                    </div>
                    <div className="player_data4">
                        <div className="player_runs">
                            <div className="runs_name">
                                <h2>Runs</h2>
                                <p className="details1">{prop.runs}</p>
                            </div>
                        </div>
                    </div>
                    <div className="player_data4">
                        <div className="player_wickes">
                            <div className="wickes_name">
                                <h2>Wickes</h2>
                                <p className="details1">{prop.wickets}</p>
                            </div>
                        </div>
                    </div>

                </div>

</div>

            </div>
            <div className="table-container">
                <table className="table">
                    <thead >
                        <tr className="table_row">
                            <th className="table_td"></th>

                            <th className="table_td">Mat</th>
                            <th className="table_td">Balls</th>
                            <th className="table_td">Overs</th>
                            <th className="table_td" >Maidens</th>
                            <th className="table_td" >RC</th>
                            <th className="table_td" >Wickets</th>
                            <th className="table_td" >Bowling Avg</th>
                            <th className="table_td" >E/R</th>
                            <th className="table_td">S/R</th>
                            <th className="table_td" >4w</th>
                            <th className="table_td" >5w</th>
                            <th className="table_td">10w</th>

                        </tr>
                    </thead>
                    <tbody className="table_body">

                        <tr className="table_row">
                            <th className="table_td"> Test</th>

                            <td className="table_td">1</td>
                            <td className="table_td">2</td>
                            <td className="table_td">3</td>
                            <td className="table_td">4</td>
                            <td className="table_td">5</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">-</td>

                        </tr>
                        <tr className="table_row">
                            <th className="table_td"> Test</th>

                            <td className="table_td">1</td>
                            <td className="table_td">2</td>
                            <td className="table_td">3</td>
                            <td className="table_td">4</td>
                            <td className="table_td">5</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">-</td>

                        </tr>
                        <tr className="table_row">
                            <th className="table_td"> Test</th>

                            <td className="table_td">1</td>
                            <td className="table_td">2</td>
                            <td className="table_td">3</td>
                            <td className="table_td">4</td>
                            <td className="table_td">5</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">-</td>

                        </tr>
                        <tr className="table_row">
                            <th className="table_td"> Test</th>

                            <td className="table_td">1</td>
                            <td className="table_td">2</td>
                            <td className="table_td">3</td>
                            <td className="table_td">4</td>
                            <td className="table_td">5</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">-</td>

                        </tr>
                        <tr className="table_row">
                            <th className="table_td"> Test</th>

                            <td className="table_td">1</td>
                            <td className="table_td">2</td>
                            <td className="table_td">3</td>
                            <td className="table_td">4</td>
                            <td className="table_td">5</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">-</td>

                        </tr>
                        <tr className="table_row">
                            <th className="table_td"> Test</th>

                            <td className="table_td">1</td>
                            <td className="table_td">2</td>
                            <td className="table_td">3</td>
                            <td className="table_td">4</td>
                            <td className="table_td">5</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">-</td>

                        </tr>
                        <tr className="table_row">
                            <th className="table_td"> Test</th>

                            <td className="table_td">1</td>
                            <td className="table_td">2</td>
                            <td className="table_td">3</td>
                            <td className="table_td">4</td>
                            <td className="table_td">5</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">6</td>
                            <td className="table_td">-</td>
                        </tr>
                    </tbody>
                </table>
            </div>


            <div className="news1">
                <div className="newsbox">
                    <h2>IND VS PAK</h2>
                    <p>ASIA CUP 2023</p>
                    <hr />
                    <div className="headnews">
                        <a href="/news"><img className="heading_img" src="https://www.cricbuzz.com/a/img/v1/420x235/i1/c344185/rahul-hits-his-stride-straight.jpg" alt="" /></a>
                        <a href="/news" className="heading_news1"><h2 className="heading_news1">Relief for India as Rahul completes seamless comeback</h2></a>
                        <p className="heading_news2">The Super Four match against Pakistan offered evidence of why India have been willing to play the risky 11th-hour waiting game with Rahul</p>
                        <p className="heading_news2">There was no false start today, however. He took his time stepping to the field of play when his partner had typically bounded out over the ropes. Fittingly, when the same pair walked back at the end of the innings, it was he that got the lead and the honour of soaking in the applause echoing from the partially-filled stands.</p>
                    </div>
                    <div className="sidebox">
                        <div className="sidebox_con">
                            <img className="side_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367078.6.jpg" alt="" />
                            <div>
                                <h4 className="n1"><strong>Kuldeep credits straighter run-up, increased pace for ODI success</strong></h4>
                                <p className="excon"> "Rhythm has become aggressive, approach is nice, maybe my hand used to fall over but that is well in control," he talks of his technical adjustments.</p>
                            </div>
                        </div>
                    </div>
                    <div className="sidebox">
                        <div className="sidebox_con">
                            <img className="side_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367059.6.jpg" alt="" />
                            <div>
                                <h4 className="n1"><strong>When menace meets precision - Jasprit Bumrah shows what he's all about</strong></h4>
                                <p className="excon"> "Bowling for the first time in ODIs since his return from back injury, he subjected the Pakistan top order to an absolute nightmare".</p>
                            </div>
                        </div>
                    </div>
                    <div className="sidebox1">
                        <div className="sidebox_con1">
                            <img className="side_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367084.6.jpg" alt="" />
                            <div>
                                <h4 className="n2"><strong>Sublime India launch their Star Destroyers into hyperdrive</strong></h4>
                                <p className="excon">They played a near-perfect match to get their engines roaring and furnaces at full blast, dispatching an off-colour Pakistan with awesome precision.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Player;