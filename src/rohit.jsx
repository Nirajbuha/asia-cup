import React from "react";
import Table from '../src/ptable'
import SwiperSlider from "./swiper";
import Player from "./playrcard";
import Playerdata from "./Playerdata";
import { useLocation } from "react-router-dom";
import Footer from "./footer";





const PlayerInfo = (props) => {
  const location = useLocation()
  const id = location.state.id
    const filteredPlayers = Playerdata.filter((val) =>val.id === id);

    return (
      <>
        {filteredPlayers.map((val) => (
          <Player
            key={val.id}
            name={val.name}
            imgsrc={val.imgsrc}
            born={val.born}
            bornplace={val.bornplace}
            battingstyle={val.battingstyle}
            bowlingstyle={val.bowlingstyle}
            nationality={val.nationality}
            age={val.age}
            role={val.role}
            team={val.team}
            role2={val.role2}
            matches={val.matches}
            runs={val.runs}
            wickets={val.wickets}
          />
          
        ))}
        <SwiperSlider/>
        <Footer />
      </>
    );
  };
  
  export default PlayerInfo;
  