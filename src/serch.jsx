import React from "react";
import './index.css';
import { Input} from 'antd';

const { Search } = Input;


const onSearch = (value) => console.log(value);

const Niraj = () => (
    <Search
      placeholder="input search text"
      allowClear
      enterButton="Search"
      size="large"
      onSearch={onSearch}
    />
);

export default Niraj ;

