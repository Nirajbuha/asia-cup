import React from "react";
import Card from './card';
import sldata from "./sdata";
import Notes from "./notes";
import Footer from "./footer";



const Sri = () =>
    <>
        <div className="cards-container">
        {sldata.map((val) => {
            return (
                
            
                <Card
                    key={val.id}
                    imgsrc={val.imgscr}
                    title={val.title}
                    sname={val.sname}
                    link={val.link}
                   
                />
             
            )
        }
    )}
 
       </div>
       <Notes />
       <Footer />
       
    </>

export default Sri;