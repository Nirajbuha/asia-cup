import React from "react";

import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectFade, Navigation, Pagination } from 'swiper/modules';
// import { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import 'swiper/css';
import 'swiper/css/bundle';
import  "swiper/css/effect-flip";
import 'swiper/css/controller';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';


const SwiperSlider = () => {
  return (
    <Swiper
      modules={[Navigation, Pagination, EffectFade]}
      // spaceBetween={30}
      slidesPerView={1}
      fadeEffect={{ crossFade: true }}
      loop={true}
      effect={"fade"}
      navigation={{clickable: true}}
      pagination={{ clickable: true }}
      scrollbar={{ draggable: true }}
      // centeredSlides={true}

      
    >
      <div className="slider">
        <SwiperSlide>
          <div className="kohli1"><img className="kohli" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367084.6.jpg" alt="" />
            <div className="kohli2">
              <h3 >Sublime India launch their Star Destroyers into hyperdrive</h3>
              <p >They played a near-perfect match to get their engines roaring and furnaces at full blast, dispatching an off-colour Pakistan with awesome precision</p></div></div></SwiperSlide>

        <SwiperSlide>
          <div className="kohli1"><img className="kohli" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367066.6.jpg" alt="" />
            <div className="kohli2">
              <h3 >Pakistan's day(s) of horror</h3>
              <p>The DJ's setlist during India-Pakistan games at neutral venues - which is all India-Pakistan games now - can often be whimsically random.</p></div>
          </div></SwiperSlide>
        <SwiperSlide>
          <div className="kohli1">
            <img className="kohli" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367053.6.jpg" alt="" />
            <div className="kohli2">
              <h3 >India's fitness to be put to test against in-form Sri Lanka</h3>
              <p >On Tuesday, those fitness levels will be put to test as India return to the R Premadasa Stadium a little over 12 hours after beating Pakistan in a game that was played over.</p></div></div></SwiperSlide>
        <SwiperSlide>
          <div className="kohli1"><img className="kohli" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367066.6.jpg " alt="" />
            <div className="kohli2">
              <h3 >Pakistan's day(s) of horror</h3>
              <p >The DJ's setlist during India-Pakistan games at neutral venues - which is all India-Pakistan games now - can often be whimsically random.</p></div></div></SwiperSlide>
      </div>
    </Swiper>



  );
};

export default SwiperSlider;
