import React from "react";

const Update = () => {
    return (
        <div>
            <h1 className="heading_home">AsiaCup Updates</h1>
            <div className="flex" style={{display:"flex"}}>
            <div className="update"><img className="update_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367066.6.jpg" alt="" />
            <div className="update_news">
              <h3 >Pakistan's day(s) of horror</h3>
              <p>The DJ's setlist during India-Pakistan games at neutral venues - which is all India-Pakistan games now - can often be whimsically random.</p></div>
          </div>
          <div className="update"><img className="update_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367000/367069.6.jpg" alt="" />
            <div className="update_news">
              <h3 >Supporting actor Hardik delivers a hit </h3>
              <p>With Sri Lanka inching towards victory, he bowled a searing spell where every ball seemed to have the batter's name</p></div>
          </div>
          <div className="update"><img className="update_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_1200,q_60/lsci/db/PICTURES/CMS/367100/367148.6.jpg" alt="" />
            <div className="update_news">
              <h3 >The joyous appeal of Dunith Wellalage</h3>
              <p>At just 20, he bowls with intensity, bats with heart, and catches like a dream. What's not to love? You can see the appeal, right? I mean, everyone can.</p></div>
          </div> 
          </div>
          <div className="flex" style={{display:"flex"}} >
          <div className="update"><img className="update_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_640,q_50/lsci/db/PICTURES/CMS/367000/367094.jpg" alt="" />
            <div className="update_news">
              <h3 >Rohit becomes the second-fastest batter to 10,000 ODI runs</h3>
              <p> A look at the staggering records and statistics that show how Rohit Sharma has excelled in ODIs.</p></div>
          </div>  <div className="update"><img className="update_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_640,q_50/lsci/db/PICTURES/CMS/367100/367151.6.jpg" alt="" />
            <div className="update_news">
              <h3 >Rohit, bowlers break SL's winning streak to put India in final</h3>
              <p>Wellalage's all-round show of five wickets and an unbeaten 42 went in vain as SL failed to chase 214.</p></div>
          </div>  <div className="update"><img className="update_img" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_wide_w_640,q_50/lsci/db/PICTURES/CMS/366900/366957.jpg" alt="" />
            <div className="update_news">
              <h3 >Shreyas Iyer still not fully fit, unavailable for SL game</h3>
              <p>hreyas Iyer's sudden back injury continues to trouble him, and he will not be .</p></div>
          </div>
          </div> 
          <div className="flex" style={{display:"flex"}} >
          <div className="update"><img className="update_img" src="https://www.hindustantimes.com/ht-img/img/2023/09/12/550x309/wellalage_1694543306831_1694543336491.jpg" alt="" />
            <div className="update_news">
              <h3 >20-year-old Dunith Wellalage rocked</h3>
              <p> India's top order getting the wickets of Shubman Gill, Rohit Sharma and Virat Kohli before completing a memorable five-wicket-haul.(Getty).</p></div>
          </div>  <div className="update"><img className="update_img" src="https://www.hindustantimes.com/ht-img/img/2023/09/12/550x309/bumrah_1694543306318_1694543348878.jpg" alt="" />
            <div className="update_news">
              <h3 >Jasprit Bumrah was on fire once again,</h3>
              <p> snuffing put two quick wickets early in Sri Lanka's chase snuffing put two quick wickets early in Sri Lanka's chase</p></div>
          </div>  <div className="update"><img className="update_img" src="https://www.hindustantimes.com/ht-img/img/2023/09/09/550x309/CRICKET-ASIA-CUP-SRI-BAN-ODI-8_1694284366803_1694284425303.jpg" alt="" />
            <div className="update_news">
              <h3 >Maheesh Theekshana celebrates after taking the wicket of Taskin Ahmed (AFP)</h3>
              <p>The DJ's setlist during India-Pakistan games at neutral venues - which is all India-Pakistan games.</p></div>
          </div>
          </div>  
        
        </div>
    )
}

export default Update;


